//
//  main.m
//  PicsBook2
//
//  Created by Yvon Valdepeñas on 06/04/14.
//  Copyright (c) 2014 Yvon Valdepeñas. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YVHAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YVHAppDelegate class]));
    }
}
