//
//  YVHViewController.h
//  PicsBook2
//
//  Created by Yvon Valdepeñas on 06/04/14.
//  Copyright (c) 2014 Yvon Valdepeñas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YVHViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end
