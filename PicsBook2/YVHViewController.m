//
//  YVHViewController.m
//  PicsBook2
//
//  Created by Yvon Valdepeñas on 06/04/14.
//  Copyright (c) 2014 Yvon Valdepeñas. All rights reserved.
//

#import "YVHViewController.h"
#import "YVHPicsViewController.h"


@interface YVHViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *photo;

@end

@implementation YVHViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
 //   [self takePhoto];
    
}



#pragma mark - Camera methods

- (IBAction)takePhoto:(id)sender {
//- (void)takePhoto {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.showsCameraControls = YES;
        picker.allowsEditing = YES;
        picker.delegate = self;
        
        //picker.cameraOverlayView = YES;
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    else {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"No uses el simulador, cacho bestia" delegate:nil cancelButtonTitle:@"Vale, vale" otherButtonTitles: nil] show];
    }
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *originalPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.photo.image = originalPhoto;
    
    UIImageWriteToSavedPhotosAlbum(originalPhoto, nil, NULL, NULL);
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - To Gallery

- (IBAction)toGallery:(id)sender {
    
    UINavigationController *picsNav = [[UINavigationController alloc]initWithRootViewController:[[YVHPicsViewController alloc]init]];
    picsNav.title = @"Pics";
    
    UINavigationController *facesNav = [[UINavigationController alloc]init];
    facesNav.title = @"Faces";
    
    UINavigationController *filtersNav = [[UINavigationController alloc]init];
    filtersNav.title = @"Filters";
    
    UINavigationController *mapNav = [[UINavigationController alloc]init];
    mapNav.title = @"Map";
    
    UINavigationController *socialNav = [[UINavigationController alloc]init];
    socialNav.title = @"Social";
    
    UINavigationController *infoNav = [[UINavigationController alloc]init];
    infoNav.title = @"Info";
    
    
    // Creamos el TabBar
    UITabBarController *tabVC = [[UITabBarController alloc] init];
    
    // Asignamos el array de controladores que ha de combinar
    [tabVC setViewControllers:@[picsNav, facesNav, filtersNav, mapNav, socialNav, infoNav]];
    
    
    [self.navigationController pushViewController:tabVC animated:YES];
    self.navigationController.navigationBarHidden = NO;

    
}

#pragma mark - Screen Orientation Control

Boolean isShowingLandscapeView;
- (void)awakeFromNib
{
    isShowingLandscapeView = NO;
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orientationChanged:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil];
}

- (void)orientationChanged:(NSNotification *)notification
{
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    if (UIDeviceOrientationIsLandscape(deviceOrientation) &&
        !isShowingLandscapeView)
    {
        [self performSegueWithIdentifier:@"DisplayAlternateView" sender:self];
        isShowingLandscapeView = YES;
    }
    else if (UIDeviceOrientationIsPortrait(deviceOrientation) &&
             isShowingLandscapeView)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        isShowingLandscapeView = NO;
    }
}


@end
